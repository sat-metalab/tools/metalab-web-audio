const audioContext = new (window.AudioContext || window.webkitAudioContext)();

const globalGain = audioContext.createGain();
globalGain.gain.value = 0.2;

const globalVolumeControl = document.querySelector('#volume');

globalVolumeControl.addEventListener('input', function() {
    globalGain.gain.value = this.value;
}, false);


const oscillator = audioContext.createOscillator();
oscillator.connect(globalGain)
globalGain.connect(audioContext.destination);

function startOscillator() {
    oscillator.start();
};
function stopOscillator() {
    oscillator.stop();
};


const audioFile = new Audio("music/adcBicycle - poor economic policies.mp3");
audioFile.loop = false;

function playFile() {
    const source = audioContext.createMediaElementSource(audioFile);
    source.connect(globalGain)
    globalGain.connect(audioContext.destination);
    audioFile.play();
};
function stopFile() {
    audioFile.currentTime = audioFile.duration;
};


const arrayBuffer = audioContext.createBuffer(2, audioContext.sampleRate * 3, audioContext.sampleRate);
for (var channel = 0; channel < arrayBuffer.numberOfChannels; channel++) {
  const nowBuffering = arrayBuffer.getChannelData(channel);
  for (let i = 0; i < arrayBuffer.length; i++) {
    nowBuffering[i] = Math.random() * 2 - 1;
  }
}

function playAudioBuffer() {
    let source = audioContext.createBufferSource();
    source.buffer = arrayBuffer;
    source.connect(globalGain)
    globalGain.connect(audioContext.destination);
    source.start();
}