import os
import ssl

from aiohttp import web

ROOT = os.path.dirname(__file__)

ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
ssl_ctx.load_cert_chain(os.path.join(ROOT, "cert.pem"), os.path.join(ROOT, 'key.pem'))

async def index(request):
    content = open(os.path.join(ROOT, "index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)

async def audionodes_page(request):
    content = open(os.path.join(ROOT, "audionodes.html"), "r").read()
    return web.Response(content_type="text/html", text=content)

async def spatialization_page(request):
    content = open(os.path.join(ROOT, "spatialization.html"), "r").read()
    return web.Response(content_type="text/html", text=content)

async def metalabwebaudio_script(request):
    content = open(os.path.join(ROOT, "../../metalabwebaudio/webscript/metalabwebaudio.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)

if __name__ == "__main__":
    app = web.Application()
    app.router.add_get("/", index)
    app.router.add_get("/audionodes.html", audionodes_page)
    app.router.add_get("/spatialization.html", spatialization_page)
    app.router.add_get("/metalabwebaudio.js", metalabwebaudio_script)

    app.router.add_static('/build/',
                    path=os.path.join(ROOT, "build"),
                    name='build')
    app.router.add_static('/css/',
                    path=os.path.join(ROOT, "css"),
                    name='css')
    app.router.add_static('/jsm/',
                    path=os.path.join(ROOT, "jsm"),
                    name='jsm')
    app.router.add_static('/models/',
                    path=os.path.join(ROOT, "models"),
                    name='models')
    app.router.add_static('/music/',
                    path=os.path.join(ROOT, "music"),
                    name='music')
    app.router.add_static('/scripts/',
                    path=os.path.join(ROOT, "scripts"),
                    name='scripts')
    app.router.add_static('/textures/',
                    path=os.path.join(ROOT, "textures"),
                    name='textures')
    web.run_app(
        app, access_log=None, host="0.0.0.0", port=8071, ssl_context=ssl_ctx
    )
