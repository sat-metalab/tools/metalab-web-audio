# How to run the tests
There are two kinds of tests available: web pages tests and unit tests.

## Web Page Tests
Theses are web pages used to confirm everything works fine with the browser.

There are two test pages. One is a simple page to test audio nodes. The other other one is is a copy-paste of the three.js [webaudio_orientation example](https://threejs.org/examples/?q=audio#webaudio_orientation), and it is used to test audio spatialization.

### 1. Prepare Metalab Web Audio
Do exactly as in the README

Note: Do not forget to open [https://0.0.0.0:8070](https://0.0.0.0:8070) with the browser to accept the certificate.

### 2. Prepare the pages
As Metalab Web Audio can be either run as a web extension or as a library, it is possible to test pages both ways.

#### option a: as a web extension
It is necessary to comment or remove the lines where the metalabwebaudio.js script is included.
```html
<!-- <script src="metalabwebaudio.js"></script> -->
```
#### option b: as a library
The script is already inserted into the pages. Nothing needs to be done.

### 2. Launch the web server
Create self signed SSL certificates
```bash
cd tests/webpages
../../util/generate_cert.sh
```
Launch the web server.
```bash
python3 webserver.py
```
### 3. Open the web pages
Open [https://0.0.0.0:8071](https://0.0.0.0:8071) with the browser and accept the certificate.

## Unit Tests
There is currently only one unit tests. It is not very elaborated and it might be hard to run because it tries to launch the SATIE server. Sorry.
