import asyncio
import os
import sys
import time

from aiortc.mediastreams import AudioStreamTrack

root_folder = os.path.dirname(__file__)
sys.path.insert(1, os.path.join(root_folder, "../../metalabwebaudio/server"))
from webaudioconnecter import WebAudioConnecter, AUDIOCONTEXT_DESTINATION_ID

async def tests():
    wac = WebAudioConnecter()
    print("actively waiting for server to boot...")
    while not wac._engine._server:
        time.sleep(0.1)
    """
    case 1 (create_nodes):
    source1    panner1    gain3
                                   gain5    destination
    source2    panner2    gain4
    """
    track1 = AudioStreamTrack()
    wac.add_track(track1)
    track2 = AudioStreamTrack()
    wac.add_track(track2)
    wac.add_node("panner", 1)
    wac.add_node("panner", 2)
    wac.add_node("gain", 3)
    wac.add_node("gain", 4)
    wac.add_node("gain", 5)

    assert len(wac._source_nodes) == 2
    assert len(wac._nodes) == 5

    source1 = wac._source_nodes[track1.id]
    source2 = wac._source_nodes[track2.id]
    panner1 = wac._nodes[1]
    panner2 = wac._nodes[2]
    gain3 = wac._nodes[3]
    gain4 = wac._nodes[4]
    gain5 = wac._nodes[5]
    
    for source in wac._source_nodes.values():
        options = wac._make_audio_object_options(source)
        assert options["is_complete"] == False
    
    assert len(wac._engine._satie.nodes) == 0

    """
    case 2 (connect_nodes):
    source1 -> panner1 -> gain3 ┓
                                -> gains5 -> destination
    source2 -> panner2 -> gain4 ┛
    """
    wac.connect_nodes(source1.id, panner1.id)
    wac.connect_nodes(panner1.id, gain3.id)
    wac.connect_nodes(gain3.id, gain5.id)

    wac.connect_nodes(source2.id, panner2.id)
    wac.connect_nodes(panner2.id, gain4.id)
    wac.connect_nodes(gain4.id, gain5.id)

    wac.connect_nodes(gain5.id, AUDIOCONTEXT_DESTINATION_ID)

    for source in wac._source_nodes.values():
        options = wac._make_audio_object_options(source)
        assert options["is_complete"] == True

    """
    case 2 (set gains):
    source1 -> panner1 -> gain3 ┓
                                -> gains5 -> destination
    source2 -> panner2 -> gain4 ┛
    """
    gain3_value = 0.2
    gain4_value = 0.4
    gain5_value = 0.6

    wac.set_gain_value(gain3.id, gain3_value)
    wac.set_gain_value(gain4.id, gain4_value)
    wac.set_gain_value(gain5.id, gain5_value)

    options1 = wac._make_audio_object_options(source1)
    assert options1["gain"] == gain3_value*gain5_value
    options2 = wac._make_audio_object_options(source2)
    assert options2["gain"] == gain4_value*gain5_value

    """
    case 3 (disconnect track):
    source1    panner1 -> gain3 ┓
                                -> gains5 -> destination
    source2 -> panner2 -> gain4 ┛
    """
    wac.disconnect_track(track1.id)
    options1 = wac._make_audio_object_options(source1)
    assert options1["is_complete"] == False
    options2 = wac._make_audio_object_options(source2)
    assert options2["is_complete"] == True

    """
    case 4 (reconnect track):
    source1 -> panner1 -> gain3 ┓
                                -> gains5 -> destination
    source2 -> panner2 -> gain4 ┛
    """
    wac.reconnect_track(track1.id)
    options1 = wac._make_audio_object_options(source1)
    assert options1["is_complete"] == False
    options2 = wac._make_audio_object_options(source2)
    assert options2["is_complete"] == True

    wac.connect_nodes(source1.id, panner1.id)
    options1 = wac._make_audio_object_options(source1)
    assert options1["is_complete"] == True
    options2 = wac._make_audio_object_options(source2)
    assert options2["is_complete"] == True

    """
    case 4 (remove track):
    source1 -> panner1 -> gain3 ┓
                                -> gains5 -> destination
               panner2 -> gain4 ┛
    """
    await wac.remove_track(track2)
    assert track2.id not in wac._nodes

    wac.close()

if __name__ == "__main__":
   asyncio.run(tests(), debug=True)