# The extension must insert javascript into the web page before the execution of any other script.
# That javascript must be inserted into the form a string.
# Modern browsers won't allow our script to read an other file in order to paste its content into the page.
# Therefore we 
# Give a look to this link to see what could have been done instead: 
# https://stackoverflow.com/questions/9515704/use-a-content-script-to-access-the-page-context-variables-and-functions/9517879#9517879

import os

root_folder = os.path.dirname(__file__)

code = """
// The javascript code we want to insert as a string
const code = `{}` ;
const scriptElement = document.createElement('script');
scriptElement.textContent = code;
// We insert the element into documentElement since head does not exist yet
document.documentElement.prepend(scriptElement);
"""

with open(os.path.join(root_folder, "webscript/metalabwebaudio.js")) as file:
    js = file.read()
    js = js.replace('`', '\\`')
    js = js.replace('${', '\${')
    code = code.format(js)

script_name = "insertmetalabwebaudio.js"
folder_path = os.path.join(root_folder, "web_extension")

try:
    os.mkdir(folder_path)
except FileExistsError:
    pass


with open(os.path.join(folder_path, script_name),"w+") as file:
    file.write(code)

manifest = """
{{
  "manifest_version": 2,
  "name": "Metalab Web Audio",
  "version": "0",

  "content_scripts": [
    {{
      "matches": ["<all_urls>"],
      "js": ["{}"],
      "run_at": "document_start"
    }}
  ]
}}
""".format(script_name)

with open(os.path.join(folder_path, "manifest.json"),"w+") as file:
    file.write(manifest)
