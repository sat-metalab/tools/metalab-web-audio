(() => {
/* 
 * Options
 */
// If true, the browser will be muted. Only Jack will produce sound
const muteBrowserSound = true;

/*
 * WebRTC
 */
class RtcManager {
    constructor() {
        this._webSocket = new WebSocket('wss://0.0.0.0:8070/ws');
        this._rtcConnection = new RTCPeerConnection();
        this._dataChannel = this._rtcConnection.createDataChannel("dataChannel");
        this._mediaStream = new MediaStream();
        this._senders = []; // we recycle senders so we don't have to renegociate
        this.messagesWaitingForNegotiation = [];
        this.messagesWaitingForDataChannel = [];

        this._webSocket.onopen = () => {
            this._configureWebSocket();
            this._configureRtcConnection();
            this._configureDataChannel();
            // must be done after everything is configured
            this._negotiateRtc();
        };
    }

    _configureWebSocket() {
        this._webSocket.onmessage = (message) => {
            const data = JSON.parse(message.data); 
            switch(data.type) {
                case "negotiation_answer":
                    this._rtcConnection.setRemoteDescription(data.answer)
                    .then(() => {
                        const oldList = this.messagesWaitingForNegotiation;
                        this.messagesWaitingForNegotiation = [];
                        oldList.forEach((gen) => {
                            if(!gen.next().done) {
                                this.messagesWaitingForNegotiation.push(gen);
                            }
                        });
                    });
                    break;
                default: 
                    break; 
            }
        };
    }

    _configureRtcConnection() {
        this._rtcConnection.onnegotiationneeded = () => {
            this._negotiateRtc();
        };

        this._rtcConnection.onicegatheringstatechange = () => {
            if (this._rtcConnection.iceGatheringState === 'complete') {
                const message = {
                    type: "negotiation_offer",
                    offer: this._rtcConnection.localDescription
                }
                this._webSocket.send(JSON.stringify(message));
                // Server will answer back with WebSocket message of type "answer"
            }
        };
    }

    _configureDataChannel() {
        this._dataChannel.onopen = () => {
            this.send = (message) => {
                this._dataChannel.send(JSON.stringify(message));
            }
            this.messagesWaitingForDataChannel.forEach((message) => {
                this.send(message);
            })
        }
    }

    _negotiateRtc() {
        this._rtcConnection.createOffer()
        .then((offer) => { 
            return this._rtcConnection.setLocalDescription(offer)
            // Will trigger _rtcConnection.onicegatheringstatechange
        });
    }

    getMid(sender) {
        // mid is use to identify tracks on both ends of the peer connection
        // MediaStreamTrack.id do not correspond
        const transceiver = this._rtcConnection.getTransceivers()
        .find( (transceiver) => {
            if (transceiver.sender == sender) {
                return transceiver;
            }
        });
        return transceiver.mid;
    }

    addTrack(track) {
        let sender = this._senders.pop();
        if (sender) {
            sender.replaceTrack(track);
            // .then(...) ?
            const message = {
                type: "reconnect_track",
                mid: this.getMid(sender)
            }
            this.send(message);
        }
        else {
            // Will trigger _rtcConnection.onnegotiationneeded
            sender = this._rtcConnection.addTrack(track, this._mediaStream);
        }
        return sender;
    }

    removeTrack(sender) {
        const message = {
            type: "disconnect_track",
            mid: this.getMid(sender)
        }
        this.send(message);
        this._senders.push(sender);
    }

    send(message) {
        // This function will be replaced immediately once this._dataChannel is available.
        this.messagesWaitingForDataChannel.push(message);
    }
}

const rtcManager = new RtcManager()

/* 
 * Audio Web API
 */

function* idGenerator() {
    let current = 1;
    while (true) {
        yield current++;
    }
}

const generateId = idGenerator();

const audioNodeFinalizer = new FinalizationRegistry( (nodeId) => {
    // We need to tell the server to delete the node in order to prevent a memory leak
    // Otherwise nodes will accumulate on server and never be garbage collected.
    const message = {
        "type": "delete_node",
        "node_id": nodeId
    };
    rtcManager.send(message);
});

class ProxyAudioParam {
    constructor(oriAudioParam, parentType, parentId) {
        this._audioParam = oriAudioParam;
        this._parentType = parentType
        this._parentId = parentId
    }

    get value() {
        return this._audioParam.value;
    }

    set value(newValue) {
        this._audioParam.value = newValue;
        const message = {
            "type": `${this._parentType}_value`,
            [`${this._parentType}_id`]: this._parentId,
            "value": parseFloat(newValue)
        }
        rtcManager.send(message)
    }

    setValueAtTime(...args) {
        this._audioParam.setValueAtTime(...args);
        // not implemented
    }

    setTargetAtTime(...args) {
        this._audioParam.setTargetAtTime(...args)
        // not implemented
    }
}

class ProxyAudioListener {
    constructor(oriAudioListener) {
        this._listener = oriAudioListener;
    }

    setOrientation(forwardX, forwardY, forwardZ, upX, upY, upZ) {
        const message = {
            "type": "audiolistener_orientation",
            "forward": {
                "x": forwardX,
                "y": forwardY,
                "z": forwardZ,
            },
            "up": {
                "x": upX,
                "y": upY,
                "z": upZ
            }
        };
        rtcManager.send(message);
        this._listener.setOrientation(forwardX, forwardY, forwardZ, upX, upY, upZ);
    }

    setPosition(positionX, positionY, positionZ) {
        const message = {
            "type": "audiolistener_position",
            "position": {
                "x": positionX,
                "y": positionY,
                "z": positionZ
            }
        };
        rtcManager.send(message);
        this._listener.setPosition(positionX, positionY, positionZ);
    }
}

function initTrackNode(audioNode) {
    // sending track to the server
    audioNode.metalabDestination = audioNode.context.createMediaStreamDestination();
    Object.getPrototypeOf(Object.getPrototypeOf(audioNode)) // getting parent's prototype
    .connect.call(audioNode, audioNode.metalabDestination); // calling super.connect for audioNode
    const track = audioNode.metalabDestination.stream.getTracks()[0]; // MediaStreamDestinations have only one track
    const sender = rtcManager.addTrack(track);

    audioNode.addEventListener('ended', () => {
        rtcManager.removeTrack(sender);
    });

    audioNode.connect = (destination, ...args) => {
        Object.getPrototypeOf(Object.getPrototypeOf(audioNode)) // getting parent's prototype
        .connect.call(audioNode, destination, ...args); // calling super.connect for audioNode

        if (!destination.id) {
            console.log("unsupported node:", destination);
            return;
        }

        // The message might be sent before the RTC negociation.
        // In that case, mid would be null and we would no be able to identify the track
        // 
        function* sending() {
            let mid;
            while ((mid = rtcManager.getMid(sender)) === null) {
                yield;
            }
            const message = {
                "type": "connect_track_to_node",
                "mid": mid,
                "destination_id": destination.id 
            };
            rtcManager.send(message);
        }
        const genSending = sending();
        rtcManager.messagesWaitingForNegotiation.push(genSending);
        // We call it once in case the negociation already occured.
        // We want to be sure not have to wait for the next one.
        genSending.next();
    }
}

const OriAudioBufferSourceNode = window.AudioBufferSourceNode;
class NewAudioBufferSourceNode extends OriAudioBufferSourceNode {
    constructor(...args) {
        super(...args);
        initTrackNode(this);
    }
}
window.AudioBufferSourceNode = NewAudioBufferSourceNode;

const OriMediaElementAudioSourceNode = window.MediaElementAudioSourceNode;
class NewMediaElementAudioSourceNode extends OriMediaElementAudioSourceNode {
    constructor(...args) {
        super(...args);
        initTrackNode(this)
    }
}
window.MediaElementAudioSourceNode = NewMediaElementAudioSourceNode;

const OriMediaStreamAudioSourceNode = window.MediaStreamAudioSourceNode;
class NewMediaStreamAudioSourceNode extends OriMediaStreamAudioSourceNode {
    constructor(...args) {
        super(...args);
        initTrackNode(this);
    }
}
window.MediaStreamAudioSourceNode = NewMediaStreamAudioSourceNode;

const OriOscillatorNode = window.OscillatorNode;
class NewOscillatorNode extends OriOscillatorNode {
    constructor(...args) {
        super(...args);
        initTrackNode(this);
    }
}
window.OscillatorNode = NewOscillatorNode;

function initCommonNode(audioNode) {
    audioNode.id = generateId.next().value;
    audioNodeFinalizer.register(audioNode, audioNode.id);

    const message = {
        "type": "add_node",
        "node_type": audioNode.type,
        "node_id": audioNode.id
    };
    rtcManager.send(message);

    audioNode.connect = (destination, ...args) => {
        Object.getPrototypeOf(Object.getPrototypeOf(audioNode)) // getting parent's prototype
        .connect.call(audioNode, destination, ...args); // calling super.connect for audioNode
        if (!destination.id) {
            console.log("unsupported node:", destination);
            return;
        }
        const message = {
            "type": "connect_nodes",
            "source_id": audioNode.id,
            "destination_id": destination.id 
        };
        rtcManager.send(message);
    }
}

const OriGainNode = window.GainNode;
class NewGainNode extends OriGainNode {
    constructor(...args) {
        super(...args);
        this.type = "gain";

        initCommonNode(this);

        const oriGain = this.gain;
        const proxyGain = new ProxyAudioParam(oriGain, this.type, this.id);
        Object.defineProperty(this, "gain", {
            get: () => { return proxyGain; }
        });
    }
}
window.GainNode = NewGainNode;

const OriPannerNode = window.PannerNode;
class NewPannerNode extends OriPannerNode {
    constructor(...args) {
        super(...args);
        this.type = "panner";

        initCommonNode(this);
    }

    set positionX(x) {
        // Creating this dummy setter activates setOrientation and setPosition.
        // The reasons of this behaviour have not been investigated yet.
    }

    setOrientation(orientationX, orientationY, orientationZ) {
        const message = {
            "type": "panner_orientation",
            "panner_id": this.id,
            "orientation": {
                "x": orientationX,
                "y": orientationY,
                "z": orientationZ
            }
        };
        rtcManager.send(message);
        super.setOrientation(orientationX, orientationY, orientationZ);
    }

    setPosition(positionX, positionY, positionZ) {
        const message = {
            "type": "panner_position",
            "panner_id": this.id,
            "position": {
                "x": positionX,
                "y": positionY,
                "z": positionZ
            }
        };
        rtcManager.send(message);
        super.setPosition(positionX, positionY, positionZ);
    }
}
window.PannerNode = NewPannerNode;

// It seems unlikely we will want to implement AnalyserNode on server side.
// The goal is not to break the node chain.
const OriAnalyserNode = window.AnalyserNode;
class NewAnalyserNode extends OriAnalyserNode {
    constructor(...args) {
        super(...args);
        this.type = "analyser";

        initCommonNode(this);
    }
}
window.AnalyserNode = NewAnalyserNode;

// It seems unlikely we will want to implement MediaStreamAudioDestinationNode on server side.
// However, the custom version of this node can be used to help understanding the node graph.
const OriMediaStreamAudioDestinationNode = window.MediaStreamAudioDestinationNode;
class NewMediaStreamAudioDestinationNode extends OriMediaStreamAudioDestinationNode {
    constructor(...args) {
        super(...args);
        this.type = "other_destination";

        initCommonNode(this);
    }
}
window.MediaStreamAudioDestinationNode = NewMediaStreamAudioDestinationNode;

const OriAudioContext = (window.AudioContext || window.webkitAudioContext);
class NewAudioContext extends OriAudioContext{
    constructor(...args) {
        super(...args);
        if (muteBrowserSound) {
            const dummyDestination = this.createMediaStreamDestination();
            Object.defineProperty(this, 'destination', { 
                get: () => { return dummyDestination; }
            });
        }
        this.destination.id = -1;

        const proxyAudioListener = new ProxyAudioListener(this.listener);
        Object.defineProperty(this, 'listener', {
            get: () => { return proxyAudioListener; }
        });
    }

    // The custom AudioNode graph can handle only one child per node. This node is often added as a second node and breaks everything. 
    // However, activate it can help to understand the exact structure of the graph.
    /* createAnalyser() {
        return new NewAnalyserNode(this);
    } */

    createBufferSource(mediaElement) {
        return new NewAudioBufferSourceNode(this);
    }

    createGain(...args) {
        return new NewGainNode(this, ...args);
    }

    createMediaElementSource(mediaElement) {
        return new NewMediaElementAudioSourceNode(this, {"mediaElement": mediaElement});
    }

    // The custom AudioNode graph can handle only one child per node. This node is often added as a second node and breaks everything. 
    // However, activate it can help to understand the exact structure of the graph.
    /* createMediaStreamDestination() {
        return new NewMediaStreamAudioDestinationNode(this);
    } */

    createMediaStreamSource(mediaStream) {
        return new NewMediaStreamAudioSourceNode(this, {"mediaStream": mediaStream});
    }

    createOscillator() {
        return new NewOscillatorNode(this);
    }

    createPanner(...args) {
        return new NewPannerNode(this, ...args);
    }
}
window.AudioContext = NewAudioContext;
window.webkitAudioContext = NewAudioContext;

})();
