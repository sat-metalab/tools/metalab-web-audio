import asyncio
import time

from jackclientmanager import JackClientManager
from satieengine import SatieEngine

AUDIOCONTEXT_DESTINATION_ID = -1 # should be configurable from the config file

class AudioNode:
    # Represents an AudioNode in Web Audio API.
    # https://developer.mozilla.org/en-US/docs/Web/API/AudioNode
    # For simplicity, here AudioNodes have only one output.
    # If they have mutiple input, these input won't merge.
    def __init__(self, node_type, node_id):
        self.type = node_type
        self.id = node_id
        self.next = None

class SourceNode (AudioNode):
    # Represents node producing sound
    # For example: AudioBufferSourceNode, OscillatorNode, MediaStreamAudioSourceNode
    def __init__(self, track_id, inport_name):
        super().__init__("root", track_id)
        # name of the port receiving sound from JACK
        # this is attributed by the spatialization engine
        self.inport_name = inport_name

class DestinationNode (AudioNode):
    # Represents the AudioContext.destination node
    # https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/destination
    def __init__(self):
        super().__init__("destination", AUDIOCONTEXT_DESTINATION_ID)


class GainNode (AudioNode):
    def __init__(self, node_id):
        super().__init__("gain", node_id)
        self.value = 1

class PannerNode (AudioNode):
    def __init__(self, node_id):
        super().__init__("panner", node_id)
        self.orientation = {"x": 0, "y": 0, "z": 0}
        self.position = {"x": 0, "y": 0, "z": 0}

class Listener:
    def __init__(self):
        self.position = {"x": 0, "y": 0, "z": 0}
        self.forward = {"x": 0, "y": 0, "z": -1}
        self.world_up = {"x": 0, "y": 1, "z": 0}

class WebAudioConnecter:
    def __init__(self, nb_updates_per_second=30, jackclient_options={}, engine_options={}):
        self._engine = SatieEngine(**engine_options)
        self._jack_client = JackClientManager(**jackclient_options)
        self._source_nodes = {} # key is node id
        self._nodes = {} # key is node id
        self._destination_node = DestinationNode()
        self._listener = Listener()
        self._nb_updates_per_second = nb_updates_per_second

        self._task = asyncio.ensure_future(self._update_engine())

    def close(self):
        self._task.cancel()
        self._jack_client.close()
        self._engine.close()

    def add_track(self, mediastream_track):
        inport_name = self._engine.use_inport()
        source_node = SourceNode(mediastream_track.id, inport_name)
        self._source_nodes[mediastream_track.id] = source_node
        self._jack_client.add_track(mediastream_track)
        self._jack_client.connect_track(source_node.id, source_node.inport_name)

    async def remove_track(self, mediastream_track):
        self._jack_client.remove_track(mediastream_track.id)
        source_node = self._source_nodes.pop(mediastream_track.id)
        self._engine.free_inport(source_node.inport_name)

    def disconnect_track(self, track_id):
        self._jack_client.disconnect_track(track_id)
        self._source_nodes[track_id].next = None
    
    def reconnect_track(self, track_id):
        # We reuse tracks to avoid WebRTC renegotiation
        # nodes are also reused for simplicity, but on browser they correspond to different nodes
        source_node = self._source_nodes[track_id]
        self._jack_client.connect_track(track_id, source_node.inport_name)

    def add_node(self, node_type, node_id):
        # use add_track to add a SourceNode
        if node_type == "gain":
            node = GainNode(node_id)
        elif node_type == "panner":
            node = PannerNode(node_id)
        else:
            node = AudioNode(node_type, node_id)
        self._nodes[node_id] = node

    def connect_nodes(self, source_id, destination_id):
        # Web Audio API terminology is ambiguous 
        # here, source does not correspond to an audio source
        source_node = self._nodes.get(source_id, None)
        if not source_node:
            source_node = self._source_nodes[source_id]
        if destination_id == AUDIOCONTEXT_DESTINATION_ID:
            destination_node = self._destination_node
        else:
            destination_node = self._nodes[destination_id]
        source_node.next = destination_node
    
    def delete_node(self, node_id):
        node = self._nodes.pop(node_id)
        node.next = None
        # A reference to the node might still exist in parent's node. We assume this does not matters
    
    def set_gain_value(self, gain_id, value):
        self._nodes[gain_id].value = value

    def set_listener_orientation(self, forward, up):
        self._listener.forward = forward
        self._listener.world_up = up
    
    def set_listener_position(self, position):
        self._listener.position = position

    def set_panner_orientation(self, panner_id, orientation):
        self._nodes[panner_id].orientation = orientation

    def set_panner_position(self, panner_id, position):
        self._nodes[panner_id].position = position

    async def _update_engine(self):
        while True:
            for source_node in self._source_nodes.values():
                audio_object_options = self._make_audio_object_options(source_node)
                self._engine.update_audio_object(audio_object_options, self._listener)
            # The number of updates per second is not accurate.
            # It does not take into account the time took for the calculus.
            # Does it matters?
            await asyncio.sleep(1/self._nb_updates_per_second)
    
    def _make_audio_object_options(self, source_node):
        options = {
            "inport_name": source_node.inport_name,
            "gain": 1,
            "position": {"x": 0, "y": 0, "z": 0},
            "is_ambiant": True, # True if the position is never modified by any panner node
            "is_complete": False # True if node branch is connected to destination
        }
        current_node = source_node
        while (current_node):
            if current_node.type == "panner":
                for key, value in current_node.position.items():
                    options["position"][key] += value
                options["is_ambiant"] = False
            elif current_node.type == "gain":
                options["gain"] *= current_node.value
            elif current_node is self._destination_node:
                options["is_complete"] = True
            current_node = current_node.next
        return options
