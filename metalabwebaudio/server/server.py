import asyncio
import argparse
import json
import os
import ssl

from aiohttp import web, WSMsgType

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaRecorder
from aiortc import rtcrtpreceiver

from webaudioconnecter import WebAudioConnecter

class Server():
    def __init__(self, host="0.0.0.0", port=8070, ssl_cert_path="cert.pem", ssl_key_path="key.pem", webaudioconnecter_options={}):
        self._rtc_connections = set()
        self._webAudioConnecter = WebAudioConnecter(**webaudioconnecter_options)
        self._init_web_app(host, port, ssl_cert_path, ssl_key_path)


    def _init_web_app(self, host, port, ssl_cert_path, ssl_key_path):
        # Be sure all the required attributes are initialized before calling this.
        # self.rtc_connections, for example
        root_folder = os.path.dirname(__file__)
        ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        ssl_ctx.load_cert_chain(os.path.join(root_folder, "cert.pem"), os.path.join(root_folder, 'key.pem'))
        app = web.Application()
        app.on_shutdown.append(self._on_shutdown)
        app.router.add_get("/", self._index)
        app.router.add_get('/ws', self._websocket_handler)
        web.run_app(
            app, access_log=None, host=host, port=port, ssl_context=ssl_ctx
        )


    async def _index(self, request):
        root_folder = os.path.dirname(__file__)
        content = open(os.path.join(root_folder, "index.html"), "r").read()
        return web.Response(content_type="text/html", text=content)


    async def _websocket_handler(self, request):
        webSocketResponse = web.WebSocketResponse()
        await webSocketResponse.prepare(request)
        rtc_connection = None

        async for message in webSocketResponse:
            if message.type == WSMsgType.TEXT:
                data = json.loads(message.data)
                if data["type"] == "negotiation_offer":
                    if not rtc_connection:
                        rtc_connection = await self._init_rtc_connection()
                    answer = await self._negotiate(data["offer"], rtc_connection)
                    await webSocketResponse.send_json({
                        "type": "negotiation_answer",
                        "answer": answer
                    })


    async def _negotiate(self, offer, rtc_connection):
        remote_description = RTCSessionDescription(sdp=offer["sdp"], type=offer["type"])
        await rtc_connection.setRemoteDescription(remote_description)
        local_description = await rtc_connection.createAnswer()
        await rtc_connection.setLocalDescription(local_description)
        return {
            "sdp": rtc_connection.localDescription.sdp,
            "type": rtc_connection.localDescription.type
        }


    async def _init_rtc_connection(self):
        rtc_connection = RTCPeerConnection()
        self._rtc_connections.add(rtc_connection)

        @rtc_connection.on("connectionstatechange")
        async def on_connectionstatechange():
            if rtc_connection.connectionState == "failed":
                await rtc_connection.close()
                self._rtc_connections.discard(rtc_connection)

        @rtc_connection.on("datachannel")
        def on_datachannel(channel):
            def find_track_from_mid(mid):
                for transceiver in rtc_connection.getTransceivers():
                    if transceiver.mid == mid:
                        return transceiver.receiver.track
            
            @channel.on("message")
            def on_message(message):
                data = json.loads(message)
                if data["type"] == "disconnect_track":
                    track = find_track_from_mid(data["mid"])
                    # Quick fix: The message to disconnect a track is sometimes received before the track has been added.
                    # The messages from the DataChannel are guaranteed to be received in order.
                    # However, the DataChannel is not responsible to add tracks.
                    # Adding a track is the job of the RTCPeerConnection, and it requires RTC negotiation.
                    # RTC negotiation takes time, and it happens asyncrously.
                    # If the track is very short (sometimes it is), it might end before the completion of the RTC negotiation.
                    # Thus, its mid will not correspond to any track on the server side.
                    # Be careful with that explanation, it might be wrong.
                    if track:
                        self._webAudioConnecter.disconnect_track(track.id)
                elif data["type"] == "reconnect_track":
                    track = find_track_from_mid(data["mid"])
                    # See "disconnect_track" comment
                    # Sometimes, short tracks are disconnected, then reconnected before they have been added properly.
                    if track:
                        self._webAudioConnecter.reconnect_track(track.id)
                elif data["type"] == "audiolistener_orientation":
                    self._webAudioConnecter.set_listener_orientation(data["forward"], data["up"])
                elif data["type"] == "audiolistener_position":
                    self._webAudioConnecter.set_listener_position(data["position"])
                elif data["type"] == "gain_value":
                    self._webAudioConnecter.set_gain_value(data["gain_id"], data["value"])
                elif data["type"] == "panner_orientation":
                    self._webAudioConnecter.set_panner_orientation(data["panner_id"], data["orientation"])
                elif data["type"] == "panner_position":
                    self._webAudioConnecter.set_panner_position(data["panner_id"], data["position"])
                elif data["type"] == "add_node":
                    self._webAudioConnecter.add_node(data["node_type"], data["node_id"])
                elif data["type"] == "connect_track_to_node":
                    track = find_track_from_mid(data["mid"])
                    self._webAudioConnecter.connect_nodes(track.id, data["destination_id"])
                elif data["type"] == "connect_nodes":
                    self._webAudioConnecter.connect_nodes(data["source_id"], data["destination_id"])
                elif data["type"] == "delete_node":
                    self._webAudioConnecter.delete_node(data["node_id"])

        @rtc_connection.on("track")
        def on_track(track):
            if track.kind == "audio":
                self._webAudioConnecter.add_track(track)

            @track.on("ended")
            async def on_ended():
                await self._webAudioConnecter.remove_track(track)

        return rtc_connection


    async def _on_shutdown(self, app):
        coros = [rtc_connection.close() for rtc_connection in self._rtc_connections]
        await asyncio.gather(*coros)
        self._rtc_connections.clear()

        self._webAudioConnecter.close()

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-c", "--config", type=str, help="Path to the configuration file")
    return parser.parse_args()

def read_config(relative_config_path):
    if not relative_config_path:
        return {"server_options": {}}
    root_folder = os.path.dirname(__file__)
    config_path = os.path.join(root_folder, relative_config_path)
    with open(config_path) as config_file:
        return json.load(config_file)

if __name__ == "__main__":
    args = parse_args()
    config = read_config(args.config)
    server = Server(**config["server_options"])
