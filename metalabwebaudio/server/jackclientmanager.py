import asyncio
from aiortc.mediastreams import MediaStreamError
import av
import jack
import numpy as np

class JackTrack:
    def __init__(self, mediastream_track):
        self._mediastream_track = mediastream_track
        self._sample_fifo = av.audio.fifo.AudioFifo()
        self._task = asyncio.ensure_future(self._run_track())

        # https://ffmpeg.org/doxygen/3.1/group__lavu__sampfmts.html
        target_audioformat = av.audio.format.AudioFormat('flt')
        # each track needs its own resampler since it notices when pts don't match
        self._resampler = av.audio.resampler.AudioResampler(target_audioformat)
    
    def get_samples(self, nb_samples):
        if (samples := self._sample_fifo.read(nb_samples)):
            resampled = self._resampler.resample(samples)
            return resampled
        else:
            return None
    
    def stop(self):
        self._task.cancel()
        #self._mediastream_track.stop() # buggy. cancel() takes too long and stopped track gets read

    async def _run_track(self):
        while True:
            try:
                frame = await self._mediastream_track.recv()
                self._sample_fifo.write(frame)
            except MediaStreamError as err:
                return

class JackClientManager:
    def __init__(self, name="MetalabJackClient"):
        self._client = self._init_jack_client(name)
        self._tracks = {} # key is track's id
        self._outports = {} # key is associated track's id
        self._new_outport_name = self._outport_name_generator()

    def close(self):
        self._client.close()
    
    def _init_jack_client(self, name):
        client = jack.Client(name)
        process_callback = self._build_process_callback()
        client.set_process_callback(process_callback)
        client.activate()
        return client

    def _build_process_callback(self):
        def process_callback(nb_samples):
            # copying dict to prevent "RuntimeError: dictionary changed size during iteration"
            tracks = self._tracks.copy()
            for track_id, track in tracks.items():
                if (samples := track.get_samples(nb_samples)):
                    self._outports[track_id].get_array()[:] = samples.to_ndarray()[0][::2]
        return process_callback

    def _outport_name_generator(self):
        i = 0
        while True:
            yield f'out_{i}'
            i += 1

    def add_track(self, mediastream_track):
        outport_name = next(self._new_outport_name)
        outport = self._client.outports.register(outport_name)
        track = JackTrack(mediastream_track)
        self._tracks[mediastream_track.id] = track
        self._outports[mediastream_track.id] = outport

    def remove_track(self, track_id):
        track = self._tracks.pop(track_id)
        track.stop()
        outport = self._outports.pop(track_id)
        outport.unregister()

    def disconnect_track(self, track_id):
        self._outports[track_id].disconnect()

    def connect_track(self, track_id, inport_name):
        outport = self._outports[track_id]
        inport = self._client.get_ports(inport_name)[0]
        try:
            outport.connect(inport)
        except jack.JackErrorCode as error:
            if error.code == 17: # connection already exists
                # Quick fix: Should have been disconnected
                pass