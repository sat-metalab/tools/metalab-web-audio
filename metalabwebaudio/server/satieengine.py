import os
import subprocess
import time

import numpy as np

from pysatie.satie import Satie

def get_listener_matrix(listener, scale):
    position_matrix = np.array([[1, 0, 0, -listener.position["x"]*scale],
                                [0, 1, 0, -listener.position["y"]*scale],
                                [0, 0, 1, -listener.position["z"]*scale],
                                [0, 0, 0, 1]])
    forward_vector = -np.array([listener.forward["x"],
                                listener.forward["y"],
                                listener.forward["z"]])
    world_up_vector = np.array([listener.world_up["x"],
                                listener.world_up["y"],
                                listener.world_up["z"]])
    right_vector = np.cross(world_up_vector, forward_vector)
    up_vector = np.cross(forward_vector, right_vector) # listener's up

    right_vector_unit = right_vector/np.linalg.norm(right_vector)    
    up_vector_unit = up_vector/np.linalg.norm(up_vector)
    forward_vector_unit = forward_vector/np.linalg.norm(forward_vector)
    
    rotation_matrix = np.array([[*right_vector_unit  , 0],
                                [*up_vector_unit     , 0],
                                [*forward_vector_unit, 0],
                                [0, 0, 0             , 1]])

    listener_matrix = np.matmul(rotation_matrix, position_matrix)
    return listener_matrix

def get_relative_position(listener, position, scale, horizontal_rotation):
    # gets the position relative to the position of the listener
    position_matrix = np.array([[1, 0, 0, position["x"]*scale],
                                [0, 1, 0, position["y"]*scale],
                                [0, 0, 1, position["z"]*scale],
                                [0, 0, 0, 1]])
    relative_matrix = np.matmul(get_listener_matrix(listener, scale), position_matrix)

    # Rotating the matrix
    offset_matrix = np.array([[np.cos(horizontal_rotation) , 0, np.sin(horizontal_rotation), 0],
                              [0                           , 1, 0                          , 0],
                              [-np.sin(horizontal_rotation), 0, np.cos(horizontal_rotation), 0],
                              [0                           , 0, 0                          , 1]])
    relative_matrix = np.matmul(offset_matrix, relative_matrix)

    relative_position = [relative_matrix[0,3], relative_matrix[1,3], -relative_matrix[2, 3]]
    
    return relative_position

class SatieEngine:
    def __init__(self,
                 launch_sc_server=True,
                 spatializer="stereoListener",
                 server_name="supernova",
                 nb_buses=32,
                 first_usable_bus=3,
                 scale=0.5,
                 horizontal_rotation=0
                ):
        self._free_buses = {} # key is inport name
        self._used_buses = {} # key is inport name
        self._server = self._launch_server(spatializer, nb_buses) if launch_sc_server else None
        self._satie = Satie()
        self._scale = scale
        self._horizontal_rotation = np.radians(horizontal_rotation)

        self._satie.initialize()
        print("Actively waiting for SATIE plugins")
        # Future versions of PySATIE should have cleaner way to achieve this
        while not self._satie.plugins:
            # Server won't receive request if it is not booten yet
            self._satie.query_audiosources()
            time.sleep(0.1)
        print("SATIE plugins received")
        
        self._generate_inport_names(server_name, nb_buses, first_usable_bus)
    
    def _launch_server(self, spatializer, nb_buses):
        code = f"""
        (
        s = Server.supernova.local;
        ~config = SatieConfiguration(s, ['\{spatializer}']);
        ~config.server.options.numInputBusChannels = {nb_buses};
        ~satie = Satie(~config);
        ~satie.config.spatializers.keys;
        ~satie.waitForBoot({{
            s.meter;
            s.plotTree;
        }});
        )
        """
        # Don't know how to pass the string directly as an argument to the process
        # The code will be copied into a file until an alternative is found
        server_script_path = os.path.join(os.path.dirname(__file__), "satieserver.scd")
        with open(server_script_path, "w+") as file:
            file.write(code)
        return subprocess.Popen(["sclang", server_script_path])
    
    def close(self):
        try:
            self._server.terminate()
        except AttributeError:
            pass

    def _generate_inport_names(self, server_name="supernova", nb_buses=32, first_usable_bus=2):
        for no_bus in range(first_usable_bus, nb_buses):
            # Associating bus to inport name
            inport_name = f'{server_name}:input_{no_bus+1}'
            self._free_buses[inport_name] = no_bus

    def use_inport(self):
        inport, bus = self._free_buses.popitem()
        self._used_buses.update({inport: bus})
        return inport

    def free_inport(self, inport_name):
        bus = self._used_buses.pop(inport_name)
        self._free_buses.update({inport_name: bus})
        if inport_name in self._satie.nodes:
            self._satie.delete_node(inport_name)

    def _create_audio_object(self, inport_name):
        no_bus = self._used_buses[inport_name]
        plugin = self._satie.get_plugin_by_name('MonoIn')
        self._satie.create_source_node(inport_name, plugin, group="default", gainDB=-2)
        self._satie.nodes[inport_name].set('bus', no_bus, 'gate', 1)

    def update_audio_object(self, options, listener):
        inport_name = options["inport_name"]
        if options["is_complete"]:
            node = self._satie._nodes.get(inport_name, None)
            if not node:
                self._create_audio_object(inport_name)
        else:
            if inport_name in self._satie.nodes:
                self._satie.delete_node(inport_name)
            return
        
        absolute_position = options["position"]

        if options["is_ambiant"]:
            # we place the source just over the listener
            for key, value in listener.world_up.items():
                absolute_position[key] += listener.position[key] + value
        
        # the distance is modified according to the gain
        gain = options["gain"]
        if (gain==0):
            gain = 0.0000000001
        scale = self._scale*1/gain

        relative_position = get_relative_position(listener, absolute_position, scale, self._horizontal_rotation)
        node = self._satie.nodes.get(inport_name, None)
        if not node:
            node = self._create_audio_object()
        node.update(relative_position)
