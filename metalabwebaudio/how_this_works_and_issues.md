# How this works
## Design
### Web Script
A script (metalabwebaudio.js) is inserted into the web page, either manually or with a browser extension (see [README](https://gitlab.com/sat-metalab/tools/metalab-web-audio/-/blob/master/README.md)). It redefines [AudioContext](https://developer.mozilla.org/en-US/docs/Web/API/AudioContext) and some other Web Audio functionnalities.

All Web Audio functionnalities still work exactly the same. The difference is that they also send informations about what they are doing to a server (see next section) through WebRTC.

### Server
The server receives informations from the web script through WebRTC. It does four things:
- Forward the audio tracks to a JACK client
- Reproduce the Web Audio Nodes and the AudioListener the best it can
- Manage SATIE
- Connect JACK client's output ports to SuperCollider (SATIE)'s input ports

# Known issues
## Audio Node Inputs and Outputs
Audio nodes can handle only one output node, and inputs don't merge. In other words, a node can have multiple parents nodes, but only one child node, and the parent nodes won't merge.

When a nodes has multiple inputs, the inputs will ignore each other and continue their way to the next nodes.

Having multiple outputs nodes and merging inputs could probably have been done. However, it is difficult to determine when to tell SATIE to create a new audio object. Also, there is currently no use case for it.

## Mono only
Stereo tracks are not handled.

Web Audio and WebRTC are able to handle tracks with multiple channels. It rather seems that the resampling is not done properly to the tracks before they are forwarded to the JACK client.

# Potential issues
## Garbage Collection
Every time an audio node is created on the web page, a corresponding node is created on the server.
We don't want them to accumulate infinitely. This would be a memory leak. 

In order to solve this problem, a [FinalizationRegistry](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry) is used. It is supposed to send a message to the server when a AudioNode is garbage collected by the browser.

This solution seems to work. However, some nodes that are expected to be garbage collected are never garbaged collected. It is not sure whether this is a Metalab Web Audio problem, or simply garbage collection being unpredictable.
