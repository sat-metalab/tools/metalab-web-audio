# Metalab Web Audio
The goal of this project is to use Web Audio with the tools made by the Metalab, such as [SATIE](https://gitlab.com/sat-metalab/SATIE) and [vaRays](https://gitlab.com/sat-metalab/vaRays).

Only SATIE is currently supported.

**Works only on Chrome.** There is currently [a bug on Firefox](https://github.com/aiortc/aiortc/issues/481). It should be usable on Firefox once it is fixed.

## Setup
### 1. Install SATIE and PySATIE
- [SATIE](https://gitlab.com/sat-metalab/SATIE)
- [PySATIE](https://gitlab.com/sat-metalab/PySATIE)

### 2. Configure JACK audio
#### 2.1 Make sure PulseAudio and JACK run together
The browser likely uses PulseAudio, while Metalab Web Audio requires JACK audio. Make sure your setup allows both PulseAudio and JACK to run at the same time. Many solutions exist for this. Suggestion: [redirect PulseAudio to JACK](https://github.com/jackaudio/jackaudio.github.com/wiki/WalkThrough_User_PulseOnJack#redirecting-pulseaudio-to-jack)

#### 2.2 Adjust JACK sample rate
JACK's sample rate must be 48000Hz.

### 3. Install Python dependencies
    pip3 install aiohttp aiortc JACK-Client numpy

### 4. Run the server
    cd metalabwebaudio/server
    ../../util/generate_cert.sh
    python3 server.py
Open [https://0.0.0.0:8070](https://0.0.0.0:8070) with the browser and accept the certificate.

A config file can be used:

    python3 server -c config/config_file.json

The `config` folder contains some examples of config files. The `example_full_options.json` uses all the options availables with their default values.

### 5. Insert the script
Metalab Web Audio can be used as a browser extension or as a library. Chose **only one** of the following options. Using both at the same time will break everything.

##### Option a: as a browser extension
Build the web extension:
    python3 metalabwebaudio/buildwebextension.py

The extension is created in the folder metalabwebaudio/web_extension.
Run it with [web-ext](https://github.com/mozilla/web-ext) or install it into a browser.
- ~~[How to install in Firefox](https://developer.mozilla.org/en-US/docs/Tools/about:debugging#extensions)~~ ([currently broken](https://github.com/aiortc/aiortc/issues/481))
- [How to install in Chrome](https://developer.chrome.com/docs/extensions/mv2/getstarted/)

Note: You might want to edit the ["matches" option](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Match_patterns) in the manifest, so the extension only applies on the selected urls.

##### Option b: as a library
Insert the web_extension/metalabwebaudio.js file into the html file, **before** any script using Web Audio. For example:

    <head>
        <script src="metalabwebaudio.js"></script>
        <script src="otherwebaudioscript.js"></script>
    </head>

The tests include [some](https://gitlab.com/sat-metalab/tools/metalab-web-audio/-/blob/master/tests/webpages/audionodes.html) [examples](https://gitlab.com/sat-metalab/tools/metalab-web-audio/-/blob/master/tests/webpages/spatialization.html).

### 6. Mute browser's sound
The sound is still playing partially on the browser with original Web Audio. Attempts to cut it completely have failed. It must be muted manually, for example on PulseAudio Volume Control, or simply by muting the tab.

## Troubleshooting
### Satie failed to boot

    The preceding error dump is for ERROR: SatieStatusWatcher:doWhenSatieRunning Satie failed to boot. Status was 'booting', should have been 'running'

It is satieengine.py not waiting properly for SuperCollider to run. This is a bug with Metalab Web Audio.

To bypass this problem, launch SuperCollider and run the SATIE server manually (a satieserver.scd file should have been created, containing the required code). Then open the config file and put the `launch_sc_server` value to `false`.

### Content Security Policy
Some web sites activate the [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) (CSP). This prevents unauthorized scripts to be executed on the web page, such as Metalab Web Audio's script.

It is possible to deactivate the CSP in the browser. For example, Chrome has the extension *Disable Content-Security-Policy*. There might be something similar in Firefox. 

If you host the web page yourself, you might try to authorize metalabwebaudio.js to run on it. It would make sense if you're using it as a library. You could also try to simply deactivate CSP on the server.

Web extensions have a [CSP option](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/content_security_policy). Don't [lose your time](https://stackoverflow.com/questions/8502307/chrome-version-18-how-to-allow-inline-scripting-with-a-content-security-policy/38554505#38554505) trying to configure it for Metalab Web Audio's web extension. This option is to authorize scripts to run on the extension itself, not the web page.
